import { Component } from '@angular/core';

@Component({
  selector: 'mf-shared-styles',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mf-shared-styles';
}
